import java.util.ArrayList;

public class ShoppingCart {

    private float totalPrice;
    private int numberOfItems;

    public float getTotalPrice() {
        return totalPrice;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void addItems(float price, int numberOfItems) {
        totalPrice += price;
        numberOfItems += numberOfItems;
    }

    public void addItems(float price) {
        totalPrice += price;
        numberOfItems += 1;
    }

    public void addItems(Item item) {
        totalPrice += item.getPrice();
        numberOfItems += 1;
    }

    public void addItems(Item... list) {
        for (Item i : list) {
            totalPrice += i.getPrice();
            numberOfItems += 1;
        }

    }
}
